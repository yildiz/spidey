/*
 *
 * This file is part of the Spidey game project, licenced under the MIT License  (MIT)
 *
 * Copyright (c) 2017 Grégory Van den Borre
 *
 * More infos available: https://www.yildiz-games.be
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 * OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.spidey.game;

import be.yildizgames.common.frame.EndFrameListener;
import be.yildizgames.spidey.item.Item;
import be.yildizgames.spidey.item.ItemCollisionListener;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Grégory Van den Borre
 */
public class Timer extends EndFrameListener implements ItemCollisionListener {

    private static final int MAX_TIME = 12000;

    private long current = MAX_TIME;

    private boolean started;

    private final List<TimerListener> listeners = new ArrayList<>();

    public Timer() {
        super();
    }

    public void start() {
        this.current = MAX_TIME;
        this.started = true;
    }

    public void stop() {
        this.started = false;
    }

    public void addTimerListener(TimerListener l) {
        this.listeners.add(l);
    }

    @Override
    protected boolean frameEnded(long l) {
        if(started) {
            current -= l;
            listeners.forEach(t -> t.timeLeft(current, MAX_TIME));
            if(current <=0) {
                listeners.forEach(TimerListener::timeOver);
            }
        }
        return true;
    }

    private void addTime(long value) {
        this.current += value;
        if(current > MAX_TIME) {
            current = MAX_TIME;
        } else if(current < 0) {
            current = 0;
        }
    }

    @Override
    public void collision(Item type, int player) {
        this.addTime(type.getTimeBonus());
    }
}
