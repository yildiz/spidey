/*
 *
 * This file is part of the Spidey game project, licenced under the MIT License  (MIT)
 *
 * Copyright (c) 2017 Grégory Van den Borre
 *
 * More infos available: https://www.yildiz-games.be
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 * OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.spidey.game;

import be.yildizgames.spidey.item.Item;
import be.yildizgames.spidey.item.Items;

import java.util.Random;

/**
 * @author Grégory Van den Borre
 */
public abstract class Difficulty {

    private final Random randomItem = new Random(2);

    private final int total = this.getFlyIteration() + this.getFruitIteration() + this.getStoneIteration();

    protected abstract int getFlyIteration();

    protected abstract int getFruitIteration();

    protected abstract int getStoneIteration();

    public final Item get() {
        int i = randomItem.nextInt(total) + 1;
        Item item;
        if(i <= this.getFlyIteration()) {
            return Items.FLY;
        } else if (i > this.getFlyIteration() && i < this.getFlyIteration() + this.getFruitIteration()) {
           return Items.FRUIT;
        } else {
            return Items.STONE;
        }
    }
}
