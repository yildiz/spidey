/*
 *
 * This file is part of the Spidey game project, licenced under the MIT License  (MIT)
 *
 * Copyright (c) 2017 Grégory Van den Borre
 *
 * More infos available: https://www.yildiz-games.be
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 * OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.spidey.game;

import be.yildizgames.module.coordinate.Relative;
import be.yildizgames.module.graphic.gui.GuiFactory;
import be.yildizgames.module.graphic.gui.container.Container;
import be.yildizgames.module.graphic.gui.image.Image;
import be.yildizgames.module.graphic.material.Material;

/**
 * @author Grégory Van den Borre
 */
public class TimerMaterialization implements TimerListener {

    private static final int NUMBER = 20;

    private final Image[] timeUnits = new Image[NUMBER];

    public TimerMaterialization(GuiFactory builder, Container parent) {
        for(int i = 0; i < timeUnits.length; i++) {
            timeUnits[i] = builder.image()
                    .withRelativeHeight(new Relative(0.05f))
                    .withRelativeWidth(new Relative(0.01f))
                    .atRelativeLeft(new Relative(0.35f + 0.015f * i))
                    .atRelativeTop(new Relative(0.05f))
                    .withBackground(Material.get("time"))
                    .build(parent);
            timeUnits[i].show();
        }
    }

    @Override
    public void timeLeft(long current, long max) {
        float bar = max / (float)NUMBER;
        int value = (int)(current / bar) - 1;
        if(value < 0) {
            value = 0;
        }
        for(int i = 0; i < NUMBER; i++) {
            if(i < value) {
                this.timeUnits[i].show();
            } else {
                this.timeUnits[i].hide();
            }
        }
    }

    @Override
    public void timeOver() {

    }
}
