/*
 * This file is part of the Spidey game project, licenced under the MIT License  (MIT)
 *
 * Copyright (c) 2017 Grégory Van den Borre
 *
 * More infos available: https://www.yildiz-games.be
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 * OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 */

package be.yildizgames.spidey.item;

import be.yildizgames.client.entity.ClientGameObject;
import be.yildizgames.common.frame.EndFrameListener;
import be.yildizgames.common.geometry.Point3D;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Grégory Van den Borre
 */
public class ItemManager extends EndFrameListener {

    private final ClientGameObject spidey;

    private final ClientGameObject gipsy;

    private final List<ItemMaterialization> items = new ArrayList<>();

    public ItemManager(ClientGameObject spidey, ClientGameObject gipsy) {
        this.spidey = spidey;
        this.gipsy = gipsy;
    }

    @Override
    protected boolean frameEnded(long l) {
        for(ItemMaterialization i : items) {
            i.move(l);
            this.computeCollisions(i);
        }
        return true;
    }

    void addItem(ItemMaterialization m) {
        this.items.add(m);
    }

    private void computeCollisions(ItemMaterialization m) {
        Point3D p = m.getPosition();
        if(p.y < -200) {
            m.deactivate();
        } else if(p.y < -145 && (Math.abs(p.x - spidey.getPosition().x) < 20)) {
            m.touched(1);
        } else if(gipsy.isVisible() && p.y < -145 && (Math.abs(p.x - gipsy.getPosition().x) < 20)) {
            m.touched(2);
        }
    }
}
