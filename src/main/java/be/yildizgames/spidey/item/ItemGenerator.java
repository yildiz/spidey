/*
 * This file is part of the Spidey game project, licenced under the MIT License  (MIT)
 *
 * Copyright (c) 2017 Grégory Van den Borre
 *
 * More infos available: https://www.yildiz-games.be
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 * OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 */

package be.yildizgames.spidey.item;

import be.yildizgames.client.entity.ClientGameObject;
import be.yildizgames.client.game.engine.ClientWorld;
import be.yildizgames.common.frame.EndFrameListener;
import be.yildizgames.common.geometry.Point3D;
import be.yildizgames.common.model.EntityId;
import be.yildizgames.common.shape.Box;
import be.yildizgames.module.graphic.material.Material;
import be.yildizgames.spidey.game.Difficulty;
import be.yildizgames.spidey.game.Easy;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Supplier;

/**
 * @author Grégory Van den Borre
 */
public class ItemGenerator extends EndFrameListener implements Supplier<ItemMaterialization>{

    private final ItemManager manager;

    private long elapsed;

    private final ClientWorld world;

    private final Random randomPosition = new Random(1000);

    private final Difficulty difficulty = new Easy();

    private final List<ItemMaterialization> items = new ArrayList<>();

    /**
     * Current type for generation.
     */
    private Item item;
    private final List<ItemCollisionListener> collisionsListeners = new ArrayList<>();
    private boolean running;

    public ItemGenerator(ClientWorld world, ItemManager manager) {
        this.world = world;
        this.manager = manager;
    }

    public void start() {
        this.running = true;
    }

    public void stop() {
        this.running = false;
        items.forEach(ItemMaterialization::hide);
    }

    @Override
    protected boolean frameEnded(long l) {
        if(!running) {
            return true;
        }
        elapsed += l;
        if(elapsed > 1000) {
            this.item = this.difficulty.get();
            ItemMaterialization mat = this.items.stream()
                    .filter(i -> i.isNotActive() && i.is(item))
                    .findFirst()
                    .orElseGet(this);
            float x = -250 + randomPosition.nextFloat() * 500;
            mat.activate(x);

            elapsed = 0;
        }
        return true;
    }

    @Override
    public ItemMaterialization get() {
        Material m;
        Box b;
        if(this.item == Items.FRUIT) {
            int i = this.randomPosition.nextInt(4) + 1;
            m = Material.get("fruit0" + i);
            if (i == 1) {
                b = Box.box(80, 80, 1);
            } else if (i == 2) {
                b = Box.box(40, 80, 1);
            } else if (i == 3) {
                b = Box.box(60, 60, 1);
            }  else if (i == 4) {
            b = Box.box(40, 80, 1);
            } else {
                b = Box.box(80, 80, 1);
            }
        } else if(this.item == Items.STONE) {
            m =  Material.get("rock01");
            b = Box.box(60, 60, 1);
        } else {
            m = Material.green();
            b = Box.box(80, 80, 1);
        }
        ClientGameObject e = world.createMovableObject(EntityId.valueOf(this.items.size() + 100L),b, m, Point3D.valueOfY(-500));
        ItemMaterialization mat = new ItemMaterialization(e, this.item, this.collisionsListeners);
        this.items.add(mat);
        this.manager.addItem(mat);
        return mat;
    }

    public void addItemCollisionListener(ItemCollisionListener l) {
        this.collisionsListeners.add(l);
    }
}
