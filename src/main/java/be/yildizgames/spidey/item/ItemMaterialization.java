/*
 * This file is part of the Spidey game project, licenced under the MIT License  (MIT)
 *
 * Copyright (c) 2017 Grégory Van den Borre
 *
 * More infos available: https://www.yildiz-games.be
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 * OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 */

package be.yildizgames.spidey.item;

import be.yildizgames.client.entity.ClientGameObject;
import be.yildizgames.common.geometry.Point3D;

import java.util.List;

/**
 * @author Grégory Van den Borre
 */
public class ItemMaterialization {

    private final ClientGameObject e;

    private final Item type;

    private final List<ItemCollisionListener> listener;

    private boolean active = false;

    private boolean touched;

    ItemMaterialization(ClientGameObject e, Item type, List<ItemCollisionListener> l) {
        this.e = e;
        this.type = type;
        this.listener = l;
    }

    void move(long l) {
        if(active) {
            e.setPosition(e.getPosition().addY(-type.getSpeed() * l));
        }
    }

    void activate(float x) {
        e.show();
        e.setPosition(x, 500, 0);
        this.active = true;
        this.touched = false;
    }

    boolean isNotActive() {
        return !this.active;
    }

    public boolean is(Item type) {
        return this.type.equals(type);
    }

    Point3D getPosition() {
        return e.getPosition();
    }

    void deactivate() {
        this.active = false;
    }

    void touched(int player) {
        if(!touched) {
            this.listener.forEach(l -> l.collision(this.type, player));
            this.deactivate();
            this.e.setPosition(Point3D.valueOfY(-500));
            this.touched = true;
        } 
    }

    void hide() {
        this.e.hide();
    }
}
