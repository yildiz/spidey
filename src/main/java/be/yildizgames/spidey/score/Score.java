/*
 *
 * This file is part of the Spidey game project, licenced under the MIT License  (MIT)
 *
 * Copyright (c) 2017 Grégory Van den Borre
 *
 * More infos available: https://www.yildiz-games.be
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 * OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.spidey.score;

import be.yildizgames.spidey.item.Item;
import be.yildizgames.spidey.item.ItemCollisionListener;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Grégory Van den Borre
 */
public class Score implements ItemCollisionListener{

    private int valueP1 = 0;

    private int valueP2 = 0;

    private List<ScoreListener> listeners = new ArrayList<>();

    public Score() {
        super();
    }

    @Override
    public void collision(Item type, int player) {
        if(player == 1) {
            valueP1 += type.getScoreValue();
            this.listeners.forEach(s -> s.scoreUpdatedP1(valueP1));
        } else if (player == 2) {
            valueP2 += type.getScoreValue();
            this.listeners.forEach(s -> s.scoreUpdatedP2(valueP2));
        }
        this.listeners.forEach(s -> s.scoreUpdatedTotal(valueP1 + valueP2));

    }

    public void reset() {
        this.valueP1 = 0;
        this.valueP2 = 0;
        this.listeners.forEach(s -> s.scoreUpdatedP1(0));
        this.listeners.forEach(s -> s.scoreUpdatedP2(0));
        this.listeners.forEach(s -> s.scoreUpdatedTotal(0));
    }

    public int getTotal() {
        return valueP1 + valueP2;
    }

    public void addListener(ScoreListener l) {
        this.listeners.add(l);
    }
}
