/*
 *
 * This file is part of the Spidey game project, licenced under the MIT License  (MIT)
 *
 * Copyright (c) 2017 Grégory Van den Borre
 *
 * More infos available: https://www.yildiz-games.be
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 * OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.spidey.gameview;

import be.yildizgames.client.entity.ClientGameObject;
import be.yildizgames.common.frame.EndFrameListener;
import be.yildizgames.common.geometry.Point3D;
import be.yildizgames.common.time.ElapsedTimeComputer;
import be.yildizgames.module.graphic.material.Material;
import be.yildizgames.module.window.input.Key;
import be.yildizgames.module.window.input.KeyboardListener;

/**
 * @author Grégory Van den Borre
 */
public class GameViewKeyboardControl extends EndFrameListener implements KeyboardListener {

    private static final float SPEED = 0.5f;

    private static final float LIMIT = 250;

    private boolean spideyLeft;

    private boolean spideyRight;

    private final ClientGameObject spidey;

    private boolean gipsyLeft;

    private boolean gipsyRight;

    private final ClientGameObject gipsy;

    private final ElapsedTimeComputer spideyAnimTime = new ElapsedTimeComputer(100);

    private final ElapsedTimeComputer gipsyAnimTime = new ElapsedTimeComputer(100);

    private int spideyAnimStep = 1;

    private int gipsyAnimStep = 1;

    public GameViewKeyboardControl(ClientGameObject spidey, ClientGameObject gipsy) {
        this.spidey = spidey;
        this.gipsy = gipsy;
    }

    @Override
    protected boolean frameEnded(long time) {
        if(spideyLeft) {
            Point3D p = spidey.getPosition();
            float x = p.x;
            if(x > - LIMIT) {
                this.spidey.setPosition(x + (time * -SPEED), p.y, p.z);
            }
            if(spideyAnimTime.isTimeElapsed()) {
                this.spideyAnimStep = this.spideyAnimStep == 2 ? 1 : 2;
                this.spidey.setMaterial(Material.get("spidey0" + this.spideyAnimStep));
            }
        } else if (spideyRight) {
            Point3D p = spidey.getPosition();
            float x = p.x;
            if(x < LIMIT) {
                this.spidey.setPosition(x + (time * SPEED), p.y, p.z);
            }
            if(spideyAnimTime.isTimeElapsed()) {
                this.spideyAnimStep = this.spideyAnimStep == 2 ? 1 : 2;
                this.spidey.setMaterial(Material.get("spidey0" + this.spideyAnimStep));
            }
        }
        if(gipsyLeft) {
            Point3D p = gipsy.getPosition();
            float x = p.x;
            if(x > - LIMIT) {
                this.gipsy.setPosition(x + (time * -SPEED), p.y, p.z);
            }
            if(gipsyAnimTime.isTimeElapsed()) {
                this.gipsyAnimStep = this.gipsyAnimStep == 2 ? 1 : 2;
                this.gipsy.setMaterial(Material.get("gipsy0" + this.gipsyAnimStep));
            }
        } else if (gipsyRight) {
            Point3D p = gipsy.getPosition();
            float x = p.x;
            if(x < LIMIT) {
                this.gipsy.setPosition(x + (time * SPEED), p.y, p.z);
            }
            if(gipsyAnimTime.isTimeElapsed()) {
                this.gipsyAnimStep = this.gipsyAnimStep == 2 ? 1 : 2;
                this.gipsy.setMaterial(Material.get("gipsy0" + this.gipsyAnimStep));
            }
        }
        return true;
    }

    @Override
    public boolean keyPressed(char c) {
        if(c == 'a') {
            this.gipsyLeft = true;
            this.gipsyRight = false;
        } else if (c == 'e') {
            this.gipsyLeft = false;
            this.gipsyRight = true;
        }
        return true;
    }

    @Override
    public void specialKeyPressed(Key key) {
        if(key == Key.LEFT) {
            this.spideyLeft = true;
            this.spideyRight = false;
        } else if (key == Key.RIGHT) {
            this.spideyLeft = false;
            this.spideyRight = true;
        }
    }

    @Override
    public void keyReleased(char key) {
        if(key =='a') {
            this.gipsyLeft = false;
        } else if (key == 'e') {
            this.gipsyRight = false;
        }
    }

    @Override
    public void specialKeyReleased(Key key) {
        if(key == Key.LEFT) {
            this.spideyLeft = false;
        } else if (key == Key.RIGHT) {
            this.spideyRight = false;
        }
    }
}
