/*
 *
 * This file is part of the Spidey game project, licenced under the MIT License  (MIT)
 *
 * Copyright (c) 2017 Grégory Van den Borre
 *
 * More infos available: https://www.yildiz-games.be
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 * OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.spidey.gameview;

import be.yildizgames.client.entity.ClientGameObject;
import be.yildizgames.module.graphic.gui.textline.TextLine;
import be.yildizgames.spidey.item.Item;
import be.yildizgames.spidey.item.ItemCollisionListener;

/**
 * @author Grégory Van den Borre
 */
public class ScoredDisplayer implements ItemCollisionListener {

    private final ClientGameObject spidey;

    private final ClientGameObject gipsy;

    private final TextLine scoredP1;
    private final TextLine scoredP2;
    private final float screenWidth;

    public ScoredDisplayer(ClientGameObject spidey, ClientGameObject gipsy, TextLine scoredP1, TextLine scoredP2, float screenWidth) {
        this.spidey = spidey;
        this.gipsy = gipsy;
        this.scoredP1 = scoredP1;
        this.scoredP2 = scoredP2;
        this.screenWidth = screenWidth;
    }

    @Override
    public void collision(Item type, int player) {
        if(player == 1) {
            if (type.getScoreValue() > 0) {
                this.scoredP1.setText("+" + type.getScoreValue());
            }
            float x = (spidey.getPosition().x + 260) / 520;
            this.scoredP1.show();
            this.scoredP1.setLeft((int) (x * screenWidth));
        } else if (player == 2) {
            if (type.getScoreValue() > 0) {
                this.scoredP2.setText("+" + type.getScoreValue());
            }
            float x = (gipsy.getPosition().x + 260) / 520;
            this.scoredP2.show();
            this.scoredP2.setLeft((int) (x * screenWidth));
        }
    }
}
