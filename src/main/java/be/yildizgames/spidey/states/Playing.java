/*
 *
 * This file is part of the Spidey game project, licenced under the MIT License  (MIT)
 *
 * Copyright (c) 2017 Grégory Van den Borre
 *
 * More infos available: https://www.yildiz-games.be
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 * OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.spidey.states;

import be.yildizgames.client.entity.ClientGameObject;
import be.yildizgames.client.game.engine.ClientWorld;
import be.yildizgames.client.game.engine.GameEngine;
import be.yildizgames.client.gamestate.GameState;
import be.yildizgames.client.gamestate.GameStateId;
import be.yildizgames.common.geometry.Point3D;
import be.yildizgames.common.model.EntityId;
import be.yildizgames.common.shape.Box;
import be.yildizgames.module.color.Color;
import be.yildizgames.module.coordinate.Relative;
import be.yildizgames.module.graphic.Font;
import be.yildizgames.module.graphic.camera.Camera;
import be.yildizgames.module.graphic.gui.textline.TextLine;
import be.yildizgames.module.graphic.material.Material;
import be.yildizgames.module.graphic.material.MaterialPass;
import be.yildizgames.spidey.game.Timer;
import be.yildizgames.spidey.game.TimerListener;
import be.yildizgames.spidey.game.TimerMaterialization;
import be.yildizgames.spidey.gameview.GameView;
import be.yildizgames.spidey.gameview.GameViewKeyboardControl;
import be.yildizgames.spidey.gameview.ScoredDisplayer;
import be.yildizgames.spidey.item.ItemCollisionListener;
import be.yildizgames.spidey.item.ItemGenerator;
import be.yildizgames.spidey.item.ItemManager;
import be.yildizgames.spidey.score.ScoreListener;
import be.yildizgames.spidey.score.ScoreMaterialization;

/**
 * @author Grégory Van den Borre
 */
public class Playing implements GameState, ScoreListener {

    private final GameView gameView;

    private final Timer timer = new Timer();
    private final ItemGenerator itemGenerator;
    private final ClientGameObject spidey;
    private final ClientGameObject gipsy;
    private final GameEngine engine;
    private final ClientGameObject background;
    private final TextLine scored;
    private final TextLine scored2;
    private final ScoreMaterialization scoreMaterialization;
    private boolean twoPlayer;

    public Playing(GameEngine engine) {
        this.engine = engine;
        ClientWorld world = engine.getActiveWorld();
        Camera cam = world.getDefaultCamera();
        cam.setPosition(Point3D.valueOfZ(500));
        this.engine.getMaterialManager().loadSimpleTexture("spidey01", "spidey01.png")
                .disableLight()
                .setTransparency(MaterialPass.Transparency.ALPHA);
        this.engine.getMaterialManager().loadSimpleTexture("spidey02", "spidey02.png")
                .disableLight()
                .setTransparency(MaterialPass.Transparency.ALPHA);
        this.engine.getMaterialManager().loadSimpleTexture("gipsy01", "gipsy01.png")
                .disableLight()
                .setTransparency(MaterialPass.Transparency.ALPHA);
        this.engine.getMaterialManager().loadSimpleTexture("gipsy02", "gipsy02.png")
                .disableLight()
                .setTransparency(MaterialPass.Transparency.ALPHA);
        this.engine.getMaterialManager().loadSimpleTexture("time", "time.png")
                .disableLight()
                .setTransparency(MaterialPass.Transparency.ALPHA);
        this.background = world.createMovableObject(
                EntityId.valueOf(10000), Box.box(600, 500, 10), Material.get("blackboard"), Point3D.valueOf(0,0, 0))
                .setRenderBehind();
        this.engine.getMaterialManager().loadSimpleTexture("fruit01", "fruit01.png")
                .disableLight()
                .setTransparency(MaterialPass.Transparency.ALPHA);
        this.engine.getMaterialManager().loadSimpleTexture("fruit02", "fruit02.png")
                .disableLight()
                .setTransparency(MaterialPass.Transparency.ALPHA);
        this.engine.getMaterialManager().loadSimpleTexture("fruit03", "fruit03.png")
                .disableLight()
                .setTransparency(MaterialPass.Transparency.ALPHA);
        this.engine.getMaterialManager().loadSimpleTexture("fruit04", "fruit04.png")
                .disableLight()
                .setTransparency(MaterialPass.Transparency.ALPHA);
        this.engine.getMaterialManager().loadSimpleTexture("rock01", "rock01.png")
                .disableLight()
                .setTransparency(MaterialPass.Transparency.ALPHA);

        this.spidey = world.createMovableObject(EntityId.valueOf(1), Box.box(80, 80, 1), Material.get("spidey01"), Point3D.valueOf(0,-150, 0));
        this.gipsy = world.createMovableObject(EntityId.valueOf(2), Box.box(80, 80,1), Material.get("gipsy01"), Point3D.valueOf(0,-150, 0));
        GameViewKeyboardControl control = new GameViewKeyboardControl(spidey, gipsy);
        this.gameView = new GameView(engine, control);
        Font scoreFont = engine.createFont("score", "Eraser.ttf", (int)(engine.getScreenSize().height * 0.1f));

        this.scoreMaterialization = new ScoreMaterialization(
                engine.getGuiManager()
                        .textLine()
                        .withFont(scoreFont)
                        .atRelativeLeft(new Relative(0.1f))
                        .atRelativeTop(new Relative(0.03f))
                        .build(this.gameView.getContainer()),
                engine.getGuiManager()
                        .textLine()
                        .withFont(scoreFont)
                        .atRelativeLeft(new Relative(0.7f))
                        .atRelativeTop(new Relative(0.03f))
                        .build(this.gameView.getContainer()));

        ItemManager manager = new ItemManager(spidey, gipsy);

        this.timer.addTimerListener(new TimerMaterialization(engine.getGuiManager(), gameView.getContainer()));

        Font scoredFont = engine.createFont("scored", "Eraser.ttf", (int)(engine.getScreenSize().height * 0.06f), Color.YELLOW);

        Font scored2Font = engine.createFont("scored2", "Eraser.ttf", (int)(engine.getScreenSize().height * 0.06f), Color.BLUE);

        this.scored = engine.getGuiManager().textLine()
                .withFont(scoredFont)
                .atRelativeTop(new Relative(0.7f))
                .build(this.gameView.getContainer());
        scored.hide();

        this.scored2 = engine.getGuiManager().textLine()
                .withFont(scored2Font)
                .atRelativeTop(new Relative(0.7f))
                .build(this.gameView.getContainer());
        scored2.hide();

        this.itemGenerator = new ItemGenerator(world, manager);
        this.itemGenerator.addItemCollisionListener(timer);
        this.itemGenerator.addItemCollisionListener(new ScoredDisplayer(spidey, gipsy, scored, scored2, engine.getScreenSize().width));

        engine.addFrameListener(manager);
        engine.addFrameListener(control);
        engine.addFrameListener(itemGenerator);
        engine.addFrameListener(timer);
    }

    void setTwoPlayer() {
        this.twoPlayer = true;
    }

    @Override
    public void activate() {
        this.engine.getEventDispatcher().setDefaultView(this.gameView);
        this.gameView.setActive(true);
        this.gameView.show();
        this.scored.hide();
        this.scored2.hide();
        this.background.show();
        this.spidey.show();
        this.spidey.setPosition(-150, this.spidey.getPosition().y, 0);
        if(twoPlayer) {
            this.scoreMaterialization.set2players();
            this.gipsy.show();
            this.gipsy.setPosition(150, this.spidey.getPosition().y, 0);
        }
        this.scoreMaterialization.show();
        this.timer.start();
        this.itemGenerator.start();
    }

    @Override
    public void deactivate() {
        this.gameView.setActive(false);
        this.gameView.hide();
        this.scoreMaterialization.hide();
        this.background.hide();
        this.spidey.hide();
        this.gipsy.hide();
        this.timer.stop();
        this.itemGenerator.stop();
        this.twoPlayer = false;
    }

    public void addCollisionListener(ItemCollisionListener l) {
        this.itemGenerator.addItemCollisionListener(l);
    }

    public void addTimerListener(TimerListener l) {
        this.timer.addTimerListener(l);
    }

    @Override
    public GameStateId getGameStateId() {
        return GameStateId.valueOf(2);
    }

    @Override
    public void scoreUpdatedP1(int score) {
        this.scoreMaterialization.scoreUpdatedP1(score);
    }

    @Override
    public void scoreUpdatedP2(int score) {
        this.scoreMaterialization.scoreUpdatedP2(score);
    }

    @Override
    public void scoreUpdatedTotal(int score) {
        this.scoreMaterialization.scoreUpdatedTotal(score);
    }
}
