/*
 *
 * This file is part of the Spidey game project, licenced under the MIT License  (MIT)
 *
 * Copyright (c) 2017 Grégory Van den Borre
 *
 * More infos available: https://www.yildiz-games.be
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 * OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.spidey.states;

import be.yildizgames.client.game.engine.GameEngine;
import be.yildizgames.client.gamestate.GameState;
import be.yildizgames.client.gamestate.GameStateId;
import be.yildizgames.module.coordinate.Relative;
import be.yildizgames.module.graphic.Font;
import be.yildizgames.module.graphic.gui.View;
import be.yildizgames.module.graphic.gui.container.Container;
import be.yildizgames.module.graphic.gui.image.Image;
import be.yildizgames.module.graphic.gui.textline.TextLine;
import be.yildizgames.module.graphic.material.Material;
import be.yildizgames.spidey.titleview.TitleKeyboardControl;
import be.yildizgames.spidey.titleview.TitleView;

/**
 * @author Grégory Van den Borre
 */
public class TitleScreen implements GameState {

    private final View view;
    private final Image arrow;
    private final Image arrow2;
    private int playerNumber = 1;

    public TitleScreen(GameEngine engine) {
        Container container = engine
                .getGuiManager()
                .container()
                .fullScreen()
                .withBackground(Material.get("blackboard"))
                .build();
        Font titleFont = engine.createFont("title", "stl.ttf", Relative.THIRD);
        Font titleGipsyFont = engine.createFont("titleGipsy", "stl.ttf", Relative.valueOf(0.05f));
        Font startFont = engine.createFont("start", "stl.ttf", Relative.valueOf(0.08f));
        Material sunMaterial = engine.getMaterialManager().createGuiMaterial("sun01.png");
        Material treeMaterial = engine.getMaterialManager().createGuiMaterial("tree01.png");
        Material arrowMaterial = engine.getMaterialManager().createGuiMaterial("arrow.png");
        Material keyMaterial = engine.getMaterialManager().createGuiMaterial("keys.png");
        engine.getGuiManager()
                .image()
                .withRelativeHeight(Relative.valueOf(0.4f))
                .withRelativeWidth(Relative.valueOf(0.2f))
                .atRelativeTop(Relative.valueOf(0.05f))
                .atRelativeLeft(Relative.valueOf(0.7f))
                .withBackground(sunMaterial)
                .build(container);
        engine.getGuiManager()
                .image()
                .withRelativeHeight(Relative.valueOf(0.3f))
                .withRelativeWidth(Relative.valueOf(0.2f))
                .atRelativeTop(Relative.valueOf(0.6f))
                .atRelativeLeft(Relative.valueOf(0.7f))
                .withBackground(keyMaterial)
                .build(container);
        engine.getGuiManager()
                .image()
                .withRelativeHeight(Relative.valueOf(0.6f))
                .withRelativeWidth(Relative.valueOf(0.3f))
                .atRelativeTop(Relative.valueOf(0.5f))
                .atRelativeLeft(Relative.valueOf(0.04f))
                .withBackground(treeMaterial)
                .build(container);
        engine.getGuiManager()
                .image()
                .withRelativeHeight(Relative.valueOf(0.4f))
                .withRelativeWidth(Relative.valueOf(0.2f))
                .atRelativeTop(Relative.valueOf(0.05f))
                .atRelativeLeft(Relative.valueOf(0.7f))
                .withBackground(sunMaterial)
                .build(container);
        TextLine title = engine.getGuiManager()
                .textLine()
                .withFont(titleFont)
                .atRelativeTop(Relative.valueOf(0.2f))
                .atRelativeLeft(Relative.QUARTER)
                .build(container);
        title.setText("SPIDEY");
        TextLine titleGipsy = engine.getGuiManager()
                .textLine()
                .withFont(titleGipsyFont)
                .atRelativeTop(Relative.valueOf(0.43f))
                .atRelativeLeft(Relative.valueOf(0.55f))
                .build(container);
        titleGipsy.setText("(et Gipsy!)");
        TextLine p1 = engine.getGuiManager()
                .textLine()
                .withFont(startFont)
                .atRelativeTop(Relative.HALF)
                .atRelativeLeft(Relative.THIRD)
                .build(container);
        p1.setText("1 joueur");
        TextLine p2 = engine.getGuiManager()
                .textLine()
                .withFont(startFont)
                .atRelativeTop(Relative.valueOf(0.6f))
                .atRelativeLeft(Relative.THIRD)
                .build(container);
        p2.setText("2 joueur");
        this.arrow = engine.getGuiManager()
                .image()
                .atRelativeLeft(Relative.valueOf(0.28f))
                .atRelativeTop(Relative.valueOf(0.52f))
                .withRelativeWidth(Relative.valueOf(0.05f))
                .withRelativeHeight(Relative.valueOf(0.05f))
                .withBackground(arrowMaterial)
                .build(container);
        //Missing set relative method in engine, build another arrow instead.
        this.arrow2 = engine.getGuiManager()
                .image()
                .atRelativeLeft(Relative.valueOf(0.28f))
                .atRelativeTop(Relative.valueOf(0.62f))
                .withRelativeWidth(Relative.valueOf(0.05f))
                .withRelativeHeight(Relative.valueOf(0.05f))
                .withBackground(arrowMaterial)
                .build(container);
        this.view = new TitleView(container, engine.getEventDispatcher());
    }


    @Override
    public void activate() {
        this.view.setActive(true);
        this.view.show();
        this.arrow2.hide();
        this.playerNumber = 1;
    }

    @Override
    public void deactivate() {
        this.view.hide();
        this.view.setActive(false);
    }

    public void addKeyboardListener(TitleKeyboardControl l) {
        this.view.addKeyboardListener(l);
    }

    public void moveArrow() {
        if(this.arrow.isVisible()) {
            this.arrow.hide();
            this.arrow2.show();
            this.playerNumber = 2;
        } else {
            this.arrow.show();
            this.arrow2.hide();
            this.playerNumber = 1;
        }
    }

    public int getPlayerNumber() {
        return playerNumber;
    }

    @Override
    public GameStateId getGameStateId() {
        return GameStateId.valueOf(1);
    }
}
