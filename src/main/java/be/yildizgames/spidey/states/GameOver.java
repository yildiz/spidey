/*
 *
 * This file is part of the Spidey game project, licenced under the MIT License  (MIT)
 *
 * Copyright (c) 2017 Grégory Van den Borre
 *
 * More infos available: https://www.yildiz-games.be
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 * OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.spidey.states;

import be.yildizgames.client.game.engine.GameEngine;
import be.yildizgames.client.gamestate.GameState;
import be.yildizgames.client.gamestate.GameStateId;
import be.yildizgames.module.color.Color;
import be.yildizgames.module.coordinate.Relative;
import be.yildizgames.module.graphic.Font;
import be.yildizgames.module.graphic.gui.container.Container;
import be.yildizgames.module.graphic.gui.textline.TextLine;
import be.yildizgames.module.graphic.material.Material;
import be.yildizgames.spidey.gameoverview.GameOverView;
import be.yildizgames.spidey.score.ScoreListener;

/**
 * @author Grégory Van den Borre
 */
public class GameOver implements GameState, ScoreListener {

    private final GameOverView view;
    private final GameEngine engine;
    private final TextLine score;

    public GameOver(GameEngine engine) {
        super();
        this.engine = engine;
        Container container =  engine
                .getGuiManager()
                .container()
                .withSize(engine.getScreenSize().width, engine.getScreenSize().height)
                .withBackground(Material.get("blackboard"))
                .build();
        Font gameOverFont = engine.createFont("gameover", "stl.ttf", Relative.FIFTH, Color.RED);
        engine.getGuiManager()
                .textLine()
                .withFont(gameOverFont)
                .atRelativeTop(Relative.THIRD)
                .atRelativeLeft(Relative.THIRD)
                .build(container)
                .setText("GAME OVER");
        this.score = engine.getGuiManager()
                .textLine()
                .withFont(Font.get("score"))
                .atRelativeTop(Relative.HALF)
                .atRelativeLeft(Relative.THIRD)
                .build(container);
        this.view = new GameOverView(container, engine.getEventDispatcher());
    }

    void setScore(int score) {
        this.score.setText("Score: " + score);
    }

    @Override
    public void activate() {
        this.view.setActive(true);
    }

    @Override
    public void deactivate() {
        this.view.setActive(false);
    }

    @Override
    public GameStateId getGameStateId() {
        return GameStateId.valueOf(3);
    }

    @Override
    public void scoreUpdatedP1(int score) {

    }

    @Override
    public void scoreUpdatedP2(int score) {

    }

    @Override
    public void scoreUpdatedTotal(int score) {
        this.score.setText("Score: " + score);
    }
}
