/*
 *
 * This file is part of the Spidey game project, licenced under the MIT License  (MIT)
 *
 * Copyright (c) 2017 Grégory Van den Borre
 *
 * More infos available: https://www.yildiz-games.be
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 * OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.spidey.states;

import be.yildizgames.client.game.engine.GameEngine;
import be.yildizgames.client.gamestate.GameState;
import be.yildizgames.common.frame.StartFrameListener;
import be.yildizgames.common.time.ElapsedTimeComputer;
import be.yildizgames.module.audio.Music;
import be.yildizgames.module.audio.Playlist;
import be.yildizgames.spidey.game.TimerListener;
import be.yildizgames.spidey.score.Score;

import java.util.Collections;
import java.util.List;

/**
 * @author Grégory Van den Borre
 */
public class GameStateManager implements TimerListener {

    private final GameState gameOver;

    private final Playing play;

    private final GameState title;
    private final GameEngine engine;
    private final Playlist gamePlaylist;
    private final Score score;
    private int numberOfPlayer;

    public GameStateManager(GameState gameOver, Playing play, GameState title, GameEngine engine, Score score) {
        this.gameOver = gameOver;
        this.play = play;
        this.title = title;
        this.engine = engine;
        this.score = score;
        this.gamePlaylist = engine.createPlaylist("game");
        List<Music> musics = List.of(
                Music.withName("mus01.ogg", "game01"),
                Music.withName("mus02.ogg", "game02"),
                Music.withName("mus03.ogg", "game03")
        );
        //Collections.shuffle(musics);
        musics.forEach(this.gamePlaylist::addMusic);
        this.gamePlaylist.playNext();
    }

    public void play() {
        this.score.reset();
        if(this.numberOfPlayer == 2) {
            this.play.setTwoPlayer();
        }
        this.play.activate();
        this.gameOver.deactivate();
        this.title.deactivate();
    }

    public void gameover() {
        this.play.deactivate();
        this.gameOver.activate();
        this.title.deactivate();
        this.engine.addFrameListener(new StartFrameListener() {

            ElapsedTimeComputer etc = new ElapsedTimeComputer(3000);

            @Override
            public boolean frameStarted() {
                if(etc.isTimeElapsed()) {
                    start();
                    return false;
                }
                return true;
            }
        });
    }

    public void start() {
        this.play.deactivate();
        this.gameOver.deactivate();
        this.title.activate();
    }

    @Override
    public void timeLeft(long current, long max) {
        //unused
    }

    @Override
    public void timeOver() {
        this.gameover();
    }

    public void setNumberOfPlayer(int numberOfPlayer) {
        this.numberOfPlayer = numberOfPlayer;
    }
}
