/*
 *
 * This file is part of the Spidey game project, licenced under the MIT License  (MIT)
 *
 * Copyright (c) 2017 Grégory Van den Borre
 *
 * More infos available: https://www.yildiz-games.be
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 * OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 *
 */

package be.yildizgames.spidey.titleview;

import be.yildizgames.client.game.engine.GameEngine;
import be.yildizgames.module.window.input.Key;
import be.yildizgames.module.window.input.KeyboardListener;
import be.yildizgames.spidey.states.GameStateManager;
import be.yildizgames.spidey.states.TitleScreen;

/**
 * @author Grégory Van den Borre
 */
public class TitleKeyboardControl implements KeyboardListener {

    private final GameStateManager stateManager;

    private final GameEngine engine;
    private final TitleScreen title;

    public TitleKeyboardControl(GameStateManager stateManager, TitleScreen title, GameEngine engine) {
        this.stateManager = stateManager;
        this.engine = engine;
        this.title = title;
    }

    @Override
    public boolean keyPressed(char c) {
        return true;
    }

    @Override
    public void specialKeyPressed(Key key) {
        if(key == Key.ENTER) {
            this.stateManager.setNumberOfPlayer(this.title.getPlayerNumber());
            this.stateManager.play();
        } else if (key == Key.DOWN || key == Key.UP) {
            this.title.moveArrow();
        } else if (key == Key.ESC) {
            engine.close();
            System.exit(0);
        }
    }
}
