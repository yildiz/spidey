/*
 * This file is part of the Spidey game project, licenced under the MIT License  (MIT)
 *
 * Copyright (c) 2017 Grégory Van den Borre
 *
 * More infos available: https://www.yildiz-games.be
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without
 * limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
 * OR COPYRIGHT  HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE  SOFTWARE.
 */

package be.yildizgames.spidey.main;

import be.yildizgames.client.game.engine.GameEngine;
import be.yildizgames.common.file.ResourcePath;
import be.yildizgames.common.model.Version;
import be.yildizgames.spidey.score.Score;
import be.yildizgames.spidey.states.GameOver;
import be.yildizgames.spidey.states.GameStateManager;
import be.yildizgames.spidey.states.Playing;
import be.yildizgames.spidey.states.TitleScreen;
import be.yildizgames.spidey.titleview.TitleKeyboardControl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Grégory Van den Borre
 */
public class EntryPoint {

    private static final Logger LOGGER = LoggerFactory.getLogger(EntryPoint.class);

    public static void main(String[] args) {
        GameEngine engine = new GameEngine(Version.alpha(1,0,0,0));
        engine.addResourcePath(ResourcePath.vfs("main", "media/resources.spk"));
        engine.getMaterialManager().loadSimpleTexture("blackboard", "board.jpg");

        Playing playing = new Playing(engine);
        GameOver gameOver = new GameOver(engine);
        TitleScreen titleScreen = new TitleScreen(engine);
        Score score = new Score();
        score.addListener(playing);
        score.addListener(gameOver);
        GameStateManager gameStateManager = new GameStateManager(gameOver, playing, titleScreen, engine, score);
        playing.addTimerListener(gameStateManager);
        playing.addCollisionListener(score);
        titleScreen.addKeyboardListener(new TitleKeyboardControl(gameStateManager, titleScreen, engine));
        gameStateManager.start();

        engine.start();
    }

}
